# -*- coding: utf-8 -*-
import sys
from collections import Counter



with open("shortest-path-distance-matrix.txt", "r") as file:
    # los datos comienzan a partir de la linea 17 (o  18 en el txt)
    matrix = file.readlines()[17:]

    for row in matrix:
        row = row.strip()
        elem = Counter(row).items()
        for k, v in elem:
            # sys.stdout.write(str(k) + "\t" + str(v) + "\n")
            print("{}\t{}".format(k, v))

