from sklearn.datasets import load_iris

iris = load_iris()

datasets = train_test_split(iris.data, iris.target, test_size=0.2)

train_data, test_data, train_labels, test_labels = datasets
