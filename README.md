# data-science-intro

## Videos de las clases prácticas

* [Práctica 10-19-21](https://drive.google.com/file/d/1TUDZ6mbXf7mLC50g8u0J9vJLpNUDcThm/view)
* [Práctica 10-19-21](https://drive.google.com/file/d/1gBP2t2bfTSP16YgjNRzleIM2MScv8Q6H/view)
* [Ejemplo PCA](https://colab.research.google.com/drive/1qgL9lJzbcUQtW3UdKyxFN136Jc5x11PO?usp=sharing)
* [Práctica 10-15-21](https://drive.google.com/file/d/1jiZA_dsEOASyWDyYZPN3qKfYO1Q0slfu/view)
* [Práctica 10-19-21](https://drive.google.com/file/d/1yAQdC41Lbc6g-HjFi9gtpTQSPTJQuqFR/view)
